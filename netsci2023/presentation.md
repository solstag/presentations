<div class="row">
<div class="column" style="padding-right:1em">

<em>NetSci2023 — 10-14 July 2023 — Vienna</em>

# A mixed-methods approach to study socio-semantic networks

Ale Abdo · LISIS · Université Gustave Eiffel

<hr>

<small>This document (cc-by): https://solstag.gitlab.io/presentations/netsci2023/</small>

</div>

<div class="column justify" style="font-size: 80%">

Goal of constructing rigorous perspectives ⇒
* Discipline and enhance observations, not directly explain reality
* Require statistical models whose representations enable qualitative reasoning
* Employ robust statistical frameworks so qualitative constructs retain meaning

Be faithful to inscriptions ⇒
* No projections before modeling
* Consider co-occurrence as hyper-nodes
* Carry the inscriptions forward... to the bitter end!

Provide iterative abstractions ⇒
* Thanks to the above
* Results that can be built upon
* A grammar of questions-perspectives

80% of the work ⇒
* Develop human-model-data interfaces that afford these tasks

</div>

Note:
qualitative brings context and tacit and experiential knowledge
challenge is to create conditions to integrate them to explicit knowledge

---

<div class="row">
<div class="column">

<small>Abdo, Cointet, Bourret, Cambrosio. <em>Domain‐topic models with chained dimensions: Charting an emergent domain of a major oncology conference</em>. JASIST, 2021</small>

<hr>

<strong>Starting point</strong>: Oncology "research front" too big and dynamic, demanded methodological progress for socio-semantic analysis

<br>

<strong>Subject</strong>: Thematic evolution of the ASCO Annual Meeting, the major global scientific conference in the field of oncology

<br>

<strong>Inscriptions</strong>: 80k conference abstracts from ASCO between 1995 and 2017


</div>
<div class="column">

<img src="https://md.cortext.net/uploads/096509d7d912e2fe761e37301.svg" width="100%">

<img src="https://md.cortext.net/uploads/096509d7d912e2fe761e37302.svg" width="100%">

</div>

----

<div class="row">

<div style="flex:75%">
<img src="https://md.cortext.net/uploads/90487e555387bf2b881a3770b.png" alt="asco network" width="100%">
</div>

<div style="flex:25%">
<img alt="asco years" src="https://md.cortext.net/uploads/90487e555387bf2b881a37731.svg" width="100%">
</div>

</div>

Note:
At level 3 we identify L3D44 et L3D48 as high growth, between major periods cut at 2006, and concerned with public health and evaluation of health technologies, including treatment regimens.

We identify these domains with the notion of "oncopolicy", the realization by oncologists that they must actively participate in public matters, and characterize the progress of its different trends in the form of subdomains.

We contextualize this growth with institutional occurrences of the last decade, and investigate how certain countries align on research on the issue.

Boring, no social component.

---

<small>Arancibia, Frickel, Abdo. <em>The political shaping of scientific fields in Argentina’s Pesticide Conflict: Evidence from domain-topic model analysis</em>. 118th ASA Annual Meeting, 2023</small>

Starting point: A decade of ethnographic research on researcher engagement with social movements

Subject: How politics helps shape research on pesticides in Argentina

Inscriptions: 3.3k bibliographic records from WebOfScience from 1990 to 2023

Notes:
The identity of a dozen key engaged researchers identified.
A corpus tentatively exhaustive, of pesticide research with participation of someone in Argentina.
Web of Science, 3.3k notices, 8k auteurs, 2k institutions, 900 revues, années 1990-2023

----

<div style="height:100%">
<img src="https://md.cortext.net/uploads/90487e555387bf2b881a3770f.svg" alt="réseau L4Ds topiques" height="100%">
</div>

Note:
L4D4 agriculture, risk, health
L4D2 exposure, genotoxicity, cell, damage
Corresponding growth

----

<div style="height:100%">
<img src="https://md.cortext.net/uploads/90487e555387bf2b881a37710.svg" alt="réseau L4Ds auteurs" height="100%">
</div>

----

Results

- connect small qualitative data and large quantitative data to understand the place and role of researchers within a scientific field

- identify questions and elements to guide further field research, such as new key authors and themes

- identify key articles, from engaged authors, with strong precocity in their associated domains, as clues to unveil the influence of researcher engagement in the field's evolution

<img alt="tableau précocité" src="https://md.cortext.net/uploads/90487e555387bf2b881a37714.png" width="100%">

Note:
Almost all engaged researchers are present in articles appearing early in their domains (max 20% of articles preceding them in both L1 and L2)

---

<div class="row">
<div class="column">
<div>
<small>

Abdo, Raimbault, Barbier, Turnheim.

<em>So many ways out! Explorations into the existence of destabilisation as a research problem beyond the domain of transitions studies</em>.

Eu-SPRI Annual Conference, 2023

</small>

<hr>

<strong>Starting point</strong>: Transitions Studies received an injunction from within to "Open up! and Pluralize!", particularly appropriate to research on destabilisation

<br>

<strong>Subject</strong>: Discover modalities of existence of the research problem of <span style="color:green">destabilisation/decline/discontinuation (DDD)</span> beyond the domain of Transitions Studies

<br>

<strong>Inscription</strong>: 36k Social Science and Humanities bibliographic records from WebOfScience from 1987 to 2022

</div>
</div>
<div class="column">

Signifiers of <span style="color:green">DDD</span> vary between domains

| Transitions | Cell biology | Tobacco control | Geopolitics |
| ---------- | -------- | -------- | -------- |
| destabilisation | senescence | reduction | state failure |
| phase-out | apoptosis | behavior change | rise-and-fall |
| decline | cell death | cessation | regime change |

<br>

→ obstacle for bibliometric/textual analysis

<br>

Scale of "beyond TS" → obstacle for qualitative analysis

<br>

⇒ mixed-methods

  </div>
</div>

Note:
Problèmatique de recherche

----

<div class="row">
<div class="column">
    
<img alt="corpus construction" src="https://md.cortext.net/uploads/90487e555387bf2b881a37721.png" width="100%">

</div>
<div class="column">

### Corpus construction

<ol class="justify" style="list-style-type: upper-roman;">

<li>Web of Science (WOS SSCI) corpus targeting the intersection of the research problem of DDD and the domain of transitions studies

   * Size initial: ~200
   
<li>Producing a lexical neighborhood with assistance from a term extraction tool (from Cortext Manager). New WOS corpus adding the extracted terms to the query

   * Size extended: ~37k
   
<li>Cooccurrence modeling of documents and terms, clustered in domains and topics

   * Number of domains: 115

<li>Manual delimitation of identified domains by pertinence to the problem

   * Number of domains: 47

<li>Final delimited corpus

   * Size delimited: ~18k

</ol>

</div>
</div>

Notes:
Qualitative work, many days of expert work reading, annotating, coding

----

<img src="https://md.cortext.net/uploads/90487e555387bf2b881a37730.png">

<div class="table-vertical">

| Scopes           | Scales       | Hows                     | Whys         |
| --------         | --------     | --------                 | --------     |
| ⋮                | ⋮            | ⋮                        | ⋮            |
| energy sector    | sector       | deliberate interventions | descriptive  |
| biodiversity     | organisation | process to measure       | evaluative   |
| brexit           | region       | lived experience         | prescriptive |
| ⋮                | ⋮            | ⋮                        | ⋮            |

</div>

Notes:
Consider neighborhoods: thematic neighborhood (topics), disciplinary (journal clusters), hierarchical (parent cluster)
For each constellation, identify its particular signifiers for DDD, then trace features of modalities

---

<ul>

* require statistical models whose representations enable qualitative reasoning

</ul>

<em>let's group things sharing similar relations to other groups of things</em>

→ Stochastic Block Models

[→ (Hierarchical) Domain-topic maps](media/domain_topic_map.html)

<ul>

* employ robust statistical frameworks so qualitative constructs retain meaning

</ul>

→ Nested Degree-Corrected Non-parametric Bayesian Stochastic Block Model (N-DC-SBM) (Peixoto, 2017)

<ul>

* results that can be built upon

</ul>

[→ (Hierarchical) Domain-chained maps](media/domain_map_authors.html)


Note:
What we've been doing is this.
SBMs are very simple and interactionist framework.

----

## How to

<div class="columns">
<div>

Choice of sources

- Incidence graph of inscriptions

Choice of a perspective dimension

- Domain-topic model (Stochastic Block Model)
- Similarity and commonality measures

</div>
<div>

Relational overview

- Domain networks

Exploration and inquiry

- Hierarchical block map (sashimi)

Choice of dimensions for perspective transfer

- Domain-chained models

Delimitation and systematic coding

- Tables and spreadsheets

</div>
<div>

Re-beat

- Return to and reassemble previous steps

Formulate

- Descriptions and narratives
- Tailored graphics and statistics
- Interpretative decoding
- Discovery 🫣

</div>
</div>

Note: no particular order

---

Abdo, Cointet, Bourret, Cambrosio. <em>Domain-topic models with chained dimensions</em>.
<br/>JASIST (2021) 🔗 https://doi.org/10.1002/asi.24606

Sashimi with Cortext Manager 🔗 https://docs.cortext.net/sashimi/

Sashimi as a Python package 🔗 https://gitlab.com/solstag/sashimi

-----

<br/>

Alexandre Hannud Abdo

✉️ abdo@member.fsf.org

LISIS · Cortext · Université Gustave Eiffel

<br/>

-----

This document (cc-by): https://solstag.gitlab.io/presentations/netsci2023/
