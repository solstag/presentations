# <span style="color:#f69838">Sun</span><span style="color:#666668">belt</span> 2023

<em>June 27 to July 1, 2023 — Portland, OR, USA</em>

![](media/insna-sunbelt-portland-or-2023-525x225-globe.png)


Ale Abdo · LISIS · Université Gustave Eiffel · France

.~´

This presentation (cc-by): https://solstag.gitlab.io/presentations/sunbelt2023/

----

![](https://md.cortext.net/uploads/90487e555387bf2b881a37704.png)

* Research infrastructure for projects in the social sciences and humanities
* Specialized in mixed-methods with textual, "socio-semantic" and geographical data
* Making friends of digital methods and computational social science :laughing:
* Hosted at LISIS and funded by INRAE, IFRIS/Labex-SITES, UGE
* In 2022: 1k users from 400 institutions, over 75 peer-reviewed publications

<div class="fragment">

<span style="font-size: 1.6em">Cortext Manager</span>
* Online service, <em>no-code</em> and gratis
* Treat data from digital/digitized traces and inscriptions
* Heterogeneous co-occurrence networks: lexical, citation, social
* Network mapping and domain-topic methods
* Geocoding and geo-visualization


</div>

https://docs.cortext.net/

---

Sashimi, a domains (abstractions of inscriptions) based mixed-methods methodology

----

### Sashimi's intuition

* goal of constructing rigorous perspectives ⇒
    * discipline and enhance observations, not directly explain reality
    * require statistical models whose representations enable qualitative reasoning
    * employ robust statistical frameworks so qualitative constructs retain meaning

* be faithful to inscriptions ⇒
    * no projections before modeling
    * consider co-occurrence as hyper-nodes
    * carry the inscriptions forward... to the bitter end!

* provide iterative abstractions ⇒
    * thanks to the above
    * results that can be built upon
    * a grammar of questions-perspectives

* 80% of the work: develop human-model-data interfaces that afford these tasks

Note:
qualitative brings context and tacit and experiential knowledge
challenge is to create conditions to integrate them to explicit knowledge

---

Abdo, Cointet, Bourret, Cambrosio. <em>Domain‐topic models with chained dimensions: Charting an emergent domain of a major oncology conference</em>. JASIST, 2021

-----

Starting point: Oncology "research front" too big and dynamic, demanded methodological progress for socio-semantic analysis

Subject: Thematic evolution of the ASCO Annual Meeting, the major global scientific conference in the field of oncology

Inscriptions: 80k conference abstracts from ASCO between 1995 and 2017

----

<div><img src="https://md.cortext.net/uploads/90487e555387bf2b881a3770b.png" alt="asco network" style="height:666px"></div>

Note:
At level 3 we identify L3D44 et L3D48 as high growth, between major periods cut at 2006, and concerned with public health and evaluation of health technologies, including treatment regimens.

We identify these domains with the notion of "oncopolicy", the realization by oncologists that they must actively participate in public matters, and characterize the progress of its different trends in the form of subdomains.

We contextualize this growth with institutional occurrences of the last decade, and investigate how certain countries align on research on the issue.

Boring, no social component.

---

Arancibia, Frickel, Abdo. <em>The political shaping of scientific fields in Argentina’s Pesticide Conflict: Evidence from domain-topic model analysis</em>. 118th ASA Annual Meeting, 2023

-----

Starting point: A decade of ethnographic research on researcher engagement with social movements

Subject: How politics helps shape research on pesticides in Argentina

Inscriptions: 3.3k bibliographic records from WebOfScience from 1990 to 2023

Notes:
The identity of a dozen key engaged researchers identified.
A corpus tentatively exhaustive, of pesticide research with participation of someone in Argentina.
Web of Science, 3.3k notices, 8k auteurs, 2k institutions, 900 revues, années 1990-2023

----

<img src="https://md.cortext.net/uploads/90487e555387bf2b881a3770f.svg" alt="réseau L4Ds topiques" style="height:666px">

Note:
L4D4 agriculture, risk, health
L4D2 exposure, genotoxicity, cell, damage
Corresponding growth

----

<img src="https://md.cortext.net/uploads/90487e555387bf2b881a37710.svg" alt="réseau L4Ds auteurs" style="height:666px">

----

Results

- connect small qualitative data and large quantitative data to understand the place and role of researchers within a scientific field

- identify questions and elements to guide further field research, such as new key authors and themes

- identify key articles, from engaged authors, with strong precocity in their associated domains, as clues to unveil the influence of researcher engagement in the field's evolution

![tableau précocité](https://md.cortext.net/uploads/90487e555387bf2b881a37714.png)

Note:
Almost all engaged researchers are present in articles appearing early in their domains (max 20% of articles preceding them in both L1 and L2)

---

Abdo, Raimbault, Barbier, Turnheim. <em>So many ways out! Explorations into the existence of destabilisation as a research problem beyond the domain of transitions studies</em>. Eu-SPRI Annual Conference, 2023

-----

Starting point: Transitions Studies received an injunction from within to "Open up! and Pluralize!", particularly appropriate to research on destabilisation

Subject: Discover modalities of existence of the research problem of destabilisation/decline/discontinuation (DDD) beyond the domain of Transitions Studies

Inscription: 36k Social Science and Humanities bibliographic records from WebOfScience from 1987 to 2022

----

Signifiers of <span style="color:green">DDD</span> vary between domains

| Transitions | Cell biology | Tobacco control | Geopolitics |
| ---------- | -------- | -------- | -------- |
| destabilisation | senescence | reduction | state failure |
| phase-out | apoptosis | behavior change | rise-and-fall |
| decline | cell death | cessation | regime change |

→ obstacle for bibliometric/textual analysis

Scale of "beyond TS" → obstacle for qualitative analysis

⇒ mixed-methods

Note:
Problèmatique de recherche

----

<div class="columns">
<div style="width:200%; position:relative; left:-40%;">
    
![corpus construction](https://md.cortext.net/uploads/90487e555387bf2b881a37721.png)

</div>
<div style="width:100%; position:relative; left:25%;">

### Corpus construction

<ol style="list-style-type: upper-roman;">

<li>Web of Science (WOS SSCI) corpus targeting the intersection of the research problem of DDD and the domain of transitions studies

   * Size initial: ~200
   
<li>Producing a lexical neighborhood with assistance from a term extraction tool (from Cortext Manager). New WOS corpus adding the extracted terms to the query

   * Size extended: ~37k
   
<li>Cooccurrence modeling of documents and terms, clustered in domains and topics

   * Number of domains: 115

<li>Manual delimitation of identified domains by pertinence to the problem

   * Number of domains: 47

<li>Final delimited corpus

   * Size delimited: ~18k

</ol>

</div>
</div>

Notes:
Qualitative work, many days of expert work reading, annotating, coding

----

<div style="position:relative; left:-17%; width:130%">
<img src="https://md.cortext.net/uploads/90487e555387bf2b881a37730.png">
</div>

Notes:
Consider neighborhoods: thematic neighborhood (topics), disciplinary (journal clusters), hierarchical (parent cluster)
For each constellation, identify its particular signifiers for DDD, then trace features of modalities

----

<img src="https://upload.wikimedia.org/wikipedia/commons/9/94/Jackpot_6000.jpg?20110421135538" height="200px">

<div class="table-vertical">

| Scopes           | Scales       | Hows                     | Whys         |
| --------         | --------     | --------                 | --------     |
| ⋮                | ⋮            | ⋮                        | ⋮            |
| energy sector    | sector       | deliberate interventions | descriptive  |
| biodiversity     | organisation | process to measure       | evaluative   |
| brexit           | region       | lived experience         | prescriptive |
| ⋮                | ⋮            | ⋮                        | ⋮            |

</div>

---

## How does

----

<!-- .slide: data-auto-animate -->

### Sashimi's intuition

* goal of constructing rigorous perspectives ⇒
    * discipline and enhance observations, not directly explain reality
    * require statistical models whose representations enable qualitative reasoning
    * employ robust statistical frameworks so qualitative constructs retain meaning

* be faithful to inscriptions ⇒
    * no projections before modeling
    * consider co-occurrence as hyper-nodes
    * carry the inscriptions forward... to the bitter end!

* provide iterative abstractions ⇒
    * thanks to the above
    * results that can be built upon
    * a grammar of questions-perspectives

* 80% of the work: develop human-model-data interfaces that afford these tasks

Note:
qualitative brings context and tacit and experiential knowledge
challenge is to create conditions to integrate them to explicit knowledge

----

<!-- .slide: data-auto-animate -->

<ul>

* require statistical models whose representations enable qualitative reasoning

</ul>

<em>let's group things sharing similar relations to other groups of things</em><!-- .element class="fragment" data-fragment-index="1"  -->

→ Stochastic Block Models<!-- .element class="fragment" data-fragment-index="2" -->

[→ (Hierarchical) Domain-topic maps](https://documents.cortext.net/5528/5528f35b0051f55b1164d3f21b7ad7de/domain-map-doc-ter-density-ter-doc-density-norm-bylevelmax-bylevelmax-scale-linear-linear-bheight-proval-hierarchical.html)<!-- .element class="fragment" data-fragment-index="4" -->

<ul>

* employ robust statistical frameworks so qualitative constructs retain meaning

</ul>

→ Nested Degree-Corrected Non-parametric Bayesian Stochastic Block Model (N-DC-SBM) (Peixoto, 2017)<!-- .element class="fragment" data-fragment-index="3" -->

<ul>

* results that can be built upon

</ul>

[→ (Hierarchical) Domain-chained maps](https://documents.cortext.net/e1b3/e1b30311cf429e6e389455fe109a292b/domain-map-doc-ext-density-ext-doc-density-norm-bylevelmax-bylevelmax-scale-linear-linear-bheight-proval-hierarchical.html)<!-- .element class="fragment" data-fragment-index="5" -->


Note:
What we've been doing is this.
SBMs are very simple and interactionist framework.

----

## How to

<div class="columns">
<div>

Choice of sources

- Incidence graph of inscriptions<!-- .element class="fragment" -->

Choice of a perspective dimension

- Domain-topic model (Stochastic Block Model)<!-- .element class="fragment" -->
- Similarity and commonality measures<!-- .element class="fragment" -->

</div>
<div>

Relational overview

- Domain networks<!-- .element class="fragment" -->

Exploration and inquiry

- Hierarchical block map (sashimi)<!-- .element class="fragment" -->

Choice of dimensions for perspective transfer

- Domain-chained models<!-- .element class="fragment" -->

Delimitation and systematic coding

- Tables and spreadsheets<!-- .element class="fragment" -->

</div>
<div>

Re-beat

- Return to and reassemble previous steps<!-- .element class="fragment" -->

Formulate

- Descriptions and narratives<!-- .element class="fragment" -->
- Tailored graphics and statistics<!-- .element class="fragment" -->
- Interpretative decoding<!-- .element class="fragment" -->
- Discovery 🫣<!-- .element class="fragment" -->

</div>
</div>

Note: no particular order

---

Abdo, Cointet, Bourret, Cambrosio. <em>Domain-topic models with chained dimensions</em>.
<br/>JASIST (2021) 🔗 https://doi.org/10.1002/asi.24606

Sashimi with Cortext Manager 🔗 https://docs.cortext.net/sashimi/

Sashimi as a Python package 🔗 https://gitlab.com/solstag/sashimi

-----

<br/>

Alexandre Hannud Abdo

🖂 abdo@member.fsf.org

LISIS · Cortext · Université Gustave Eiffel

<br/>

-----

This presentation (cc-by): https://solstag.gitlab.io/presentations/sunbelt2023/
