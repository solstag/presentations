# Ale's reveal.js presentations

A repository to store and publish my reveal.js presentations.

Each folder corresponds to a presentation and gets published as `https://solstag.gitlab.io/presentations/{FOLDER_NAME}`

## Running the presentation server locally, with Guix
Set variables
```
export REVEAL_VERSION={}
export REVEAL_FILE=${REVEAL_VERSION}.tar.gz
export REVEAL_DIR=/home/eris/projects/src/presentations/_reveal/
export PRESENTATION_DIR=/home/eris/projects/src/presentations/{}
```
Download and extract files
```
cd ${REVEAL_DIR}
wget https://github.com/hakimel/reveal.js/archive/refs/tags/${REVEAL_FILE}
tar zxf ${REVEAL_FILE}
mv reveal-${REVEAL_VERSION} ${REVEAL_VERSION}
cd ${REVEAL_VERSION}
```
Install reveal with npm
```
guix shell node coreutils -CNF --expose=${PRESENTATION_DIR} \
-- npm install
```
Then run the server
```
guix shell node coreutils -CNF \
--expose=${PRESENTATION_DIR}  --preserve={PRESENTATION_DIR} \
-- npm start -- --root=${PRESENTATION_DIR}
```
